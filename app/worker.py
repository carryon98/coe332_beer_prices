from hotqueue import HotQueue
from PIL import Image
import jobs, datetime, json, api, redis, time, slack, io, os 
import matplotlib.pyplot as plt
import numpy as np

token = os.environ["SLACK_API_TOKEN"]                                   # uses api token from docker compose
rd    = redis.StrictRedis(host='172.17.0.1', port=6379, db=0)
q     = HotQueue('queue', host='172.17.0.1', port=6379, db=1)
plots = redis.StrictRedis(host='172.17.0.1', port=6379, db=2)
data = json.load(open('data.json', 'r'))
client = slack.WebClient(token)



def make_histogram(jid, year, price_per_oz):
    plt.figure()
    plt.hist(price_per_oz,5)
    plt.title('Histogram of Prices per Ounce ({})'.format(year))
    plt.xlabel('$ per oz.')
    plt.savefig('/tmp/plot.png', dpi=150)
    save_plot_to_redis(jid)


def save_plot_to_redis(jid):
    fb = open('/tmp/plot.png', 'rb').read()
    plots.set(jid, fb)
    send_plot_to_slack(jid)


def send_plot_to_slack(jid):
    plot_bytes = plots.get(jid)                                             # get plot from redis db=2
    image = Image.open(io.BytesIO(plot_bytes))                              # convert bytes to image
    fname = 'Plot_{}.png'.format(jid[0:7])  
    image.save(fname)
    response = client.files_upload(channels='#final_project', file=fname, title='Enjoy your plot!')
    assert response['ok']


def get_data_by_year(year1, year2):
    dataset = list(filter(lambda x: int(x['Year']) >= int(year1) and int(x['Year']) <= int(year2), data))
    return dataset


def make_plot(jid, team, year_range, price_per_oz):
    plt.figure()
    plt.plot(year_range, price_per_oz, label='{}'.format(team))
    plot_mlb_average(year_range)
    plt.legend()
    plt.xticks(np.arange(year_range[0], (year_range[-1]+1), step=1))
    plt.title('Price for {} from {} to {}'.format(team, year_range[0], year_range[-1]))
    plt.xlabel('Years')
    plt.ylabel('Price per Oz. ($)')
    plt.savefig('/tmp/plot.png', dpi=150)
    save_plot_to_redis(jid)


def plot_mlb_average(year_range):
    dataset = get_data_by_year(year_range[0], year_range[-1])
    price_per_oz = [x['Price per Ounce'] for x in dataset if x['Team']=='MLB Average' and x['Year'] in year_range]
    plt.plot(year_range, price_per_oz, label='MLB Average')


@q.worker
def execute_job(jid):
    jobs.update_job_status(jid, 'in progress')
    # update status of job using function from jobs.py
    job = jobs.idtranslate(jid)                                         # job_dict
    
    if job['start'] == job['end']:                                      # make a histogram from one year
        year = str(job['start'])
        price_per_oz = []
        for dictionary in data:
            for key,value in dictionary.items():
                if str(value) == str(year):
                    price_per_oz.append(dictionary['Price per Ounce'])
        make_histogram(jid, year, price_per_oz)  
          
    elif job['start'] < job['end']:
        year1 = job['start']
        year2 = job['end']
        team = job['team']
        dataset = get_data_by_year(year1, year2)
        year_range = list(set([x['Year'] for x in dataset]))
        year_range.sort()
        price_per_oz = [x['Price per Ounce'] for x in dataset if x['Team'] == team and x['Year'] in year_range]        
        make_plot(jid, team, year_range, price_per_oz)
     
    jobs.update_job_status(jid,'complete')
    #update status of job to 'complete' using jobs.py function
   
execute_job()

