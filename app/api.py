from flask import Flask, jsonify, request, abort, Response, current_app
from hotqueue import HotQueue
import json, uuid, redis, jobs, logging


app = Flask(__name__)
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True        # makes the terminal output cleaner
data = json.load(open('data.json','r'))                 # loads data


@app.route('/')
def all_data():
    return jsonify(data)


@app.route('/years')                                     # returns a list of the years studied
def get_years_studied():
    years = list(set([x['Year'] for x in data]))
    years.sort()
    return jsonify(years)


@app.route('/<int:year>')                               # returns all information for the given year
def get_all_from_year(year):
    dataset = list(filter(lambda x: x['Year'] == year, data))
    return jsonify(dataset)


@app.route('/<int:year>/<string:name>')                 # returns information for the given year and team
def get_info_by_year_by_team(year, name):
    dataset = list(filter(lambda x: x['Year'] == year and x['Team'] == name, data))
    return jsonify(dataset[0])


@app.route('/teams')                                    # returns a list of all teams studied
def get_teams_list():
    teams = [x['Team'] for x in data]
    teams = list(set(teams))
    return jsonify(teams)


@app.route('/<string:name>')
def get_all_from_team(name):
    dataset = list(filter(lambda x: x['Team'] == name, data))
    return jsonify(dataset)


@app.route('/jobs', methods = ['GET','POST'])
def Jobs():
    if request.method == 'POST':
        try:
            job=request.get_json(force=True)                        # get user input in json object 
            job['task'] = job.get('task').lower()
            job['start'] = int(job.get('start')) 
            job['end'] = int(job.get('end'))
        
            if job.get('start') > job.get('end'):
                raise ValueError('Incorrect date format > Start year greater than end year')                
  
            if 'team' not in job and job.get('start') != job.get('end'):
                raise ValueError('Please specify a team and try again. Ex: "Boston Red Sox"')
            if 'team' not in job and job.get('start') == job.get('end'):              
                result = jobs.add_job(job.get('start'), job.get('end'), job.get('task'))
                return jsonify({'Success': 'Histogram generation in progress...', 'result': result})
            
            if 'team' in job and job.get('start') == job.get('end'):
                raise ValueError('Please specify a range of years. Ex: "start": 2013, "end": 2018')
            if 'team' in job and job.get('start') != job.get('end'):
                result = jobs.add_job(job.get('start'), job.get('end'), job.get('task'), job.get('team'))
                return jsonify({'Success': 'Plot generation in progress...', 'result': result})

          
        except:
            return "Invalid JSON input. Please specify either a team name or a range of years."
    
    if request.method == 'GET':
        return jsonify(jobs.get_jobs())

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
