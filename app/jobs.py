from uuid import uuid4
from flask import Flask, jsonify, request, abort, Response
from hotqueue import HotQueue
import redis, os


rd   = redis.StrictRedis(host='172.17.0.1', port=6379, db=0) 
q    = HotQueue('queue', host='172.17.0.1', port=6379, db=1)
plot = redis.StrictRedis(host='172.17.0.1', port=6379, db=2)


def generate_jid():
    # generates a unique, never changing job id
    # called in add_job, nowhere else
    return str(uuid4())


def generate_job_key(jid):
    # generates a key which will have a job object as its value
    # the job object value is contains the job unique id (jid) and status
    # this means that this function will be called anytime a job is changed
    return 'job.{}'.format(jid)


def instantiate_job(jid, start, end, task, team, status):
    # instantiates a job dictionary
    # called in add_job, update job.
    if type(jid) == str:
        return {'id': jid,
                'team': team,
                'status': status,
                'start': start,
                'end': end,
                'task': task}

    return {'id': jid.decode('utf-8'),
            'team': team.decode('utf-8'),
            'status': status.decode('utf-8'),
            'start': start.decode('utf-8'),
            'end': end.decode('utf-8'),
            'task': task.decode('utf-8')}


def add_job(start, end, task, team='N/A', status='submitted'):
    # Creates a job from scratch with parameters 
    # called from api post endpoint/s
    jid = generate_jid()                                                # Generates a job id
    job_dict = instantiate_job(jid, start, end, task, team, status)     # uses that jid to create job object
    save_job(generate_job_key(jid), job_dict)                           # saves the job to redis jobs db=0
    queue_job(jid)                                                      # adds to the queue db=1
    return job_dict                                                     # returns the job info, the value in the 
                                                                        # redis jobs db=0 

def save_job(job_key, job_dict):
    # Save job dictionary with unique in the Redis db=0
    # called in add_job, update_job_status
    rd.hmset(job_key, job_dict)


def queue_job(jid):
    # Adds recently generated job key to queue db=1
    # called in add_job, nowhere else
    ##q.put(generate_job_key(jid))
    q.put(jid)


def get_all_jobs_in_rd():
    job_list = []
    keys = rd.keys()
    for key in keys:
        key = key.decode('utf-8')
        job_list.append(key)
    return job_list


def get_jobs():
    job_list = []
    for job_key in rd.keys():
        jid, start, end, task, team, status = rd.hmget(job_key, 'id', 'start', 'end', 'task', 'team', 'status')
        job = instantiate_job(jid, start, end, task, team, status)
        job_list.append(job)
    return job_list


def idtranslate(jid):
    job_key = generate_job_key(jid)
    return { 'id': rd.hget(job_key,'id').decode('utf-8'),
             'team': rd.hget(job_key, 'team').decode('utf-8'),
             'status': rd.hget(job_key,'status').decode('utf-8'),
             'start': rd.hget(job_key,'start').decode('utf-8'),
             'end': rd.hget(job_key,'end').decode('utf-8'),
             'task': rd.hget(job_key,'task').decode('utf-8') }



def update_job_status(jid, new_status):
    # Update the status of job new_status
    # pulls current job from rd db=0
    job_key = generate_job_key(jid)
       
    jid = rd.hget(job_key, 'id')
    start = rd.hget(job_key, 'start')
    end = rd.hget(job_key, 'end')
    task = rd.hget(job_key, 'task')
    team = rd.hget(job_key, 'team')
    status = rd.hget(job_key, 'status')
    jid = jid.decode('utf-8')

    job_dict = instantiate_job(jid, start, end, task, team, status)
    job_dict['status'] = new_status
    save_job(job_key, job_dict)
    

