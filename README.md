## Overview

This API is to allow submission and review of the historical MLB beer price database -- to create, update, view, and delete requests and reviews. The API specification is RESTful in that it uses HTTP methods (GET, POST, PUT, and DELETE) to interact with the allocations database. The HTTP responses are always in JSON format. The API supports multiple allocations processes, where allocations processes are different clients. Customers get charged $0.20 per GB of data used.  


## Endpoints

returns a list of the years studied:  
``` GET /years ```

returns all information for the given year:  
``` GET /<year> ```

returns a list of the teams studied:  
``` GET /teams ```

returns all information for the given teams:  
``` GET /<teamname> ```

returns information for the given year and team:  
``` GET /<year>/<teamname> ```

returns a list of all the completed jobs:  
``` GET /jobs ```

## Instructions

To run this application follow the steps below:

1. Clone and navigate to this repository:  
``` git clone https://bitbucket.org/carryon98/coe332_beer_prices.git ```

2. Build and run the image:  
``` docker-compose up -d --build ```

3. Navigate to the following Slack URL. All generated plots will be displayed here:  
- https://coe332-spring2019.slack.com/messages/GJUGZ4PL0/details/

4. To get the endpoints from above, run one of the following:  

    a. get a list of the years studied:  
    ``` curl localhost:5000/years ```  
    
    b. get all information for the given year:  
    ``` curl localhost:5000/<year> ```  
    -- ex: ``` curl localhost:5000/2018 ```
    
    c. get a list of the teams studied:  
    ``` curl localhost:5000/teams ```  
    
    d. get all information for the given team:  
    ``` curl localhost:5000/<team name> ```  
    -- ex: ``` curl localhost:5000/Houston%20Astros ```
     
    e. get information for the given year and team:  
    ``` curl localhost:5000/<year>/<team name> ```  
    -- ex: ``` curl localhost:5000/2018/Houston%20Astros ```
    
 5. Submit a job with a starting year, ending year, and task:
 
    -- Some examples:

    Generate a histogram of prices across the league for one year:  
    ``` curl -X POST -H "Content-Type: application/json" -d '{"start": "2014", "end": "2014", "task": "plot"}' localhost:5000/jobs ```
    
    Create a plot of one team's prices vs. the league average over time:  
    ``` curl -X POST -H "Content-Type: application/json" -d '{"team": "Boston Red Sox", "start": "2013", "end": "2018", "task": "plot"}' localhost:5000/jobs ```
    
## How it works
* Flask API takes user input in the form of curl commands
* If the API recognizes the command, it calls jobs.py to instantiate a job object
* Upon creation of this job object, jobs.py adds the job key to the queue
* The worker pulls this job key off of the queue, and generates plots based on the user input
* The plots are first saved to a redis database, then uploaded to Slack through the Slack Python API

## Team
- George Newman
- Anthony Carreon
- Akksay Singh
   
